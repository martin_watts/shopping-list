import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import Head from 'next/head';
import React from 'react';
import thunk from 'redux-thunk';

import reducer from '../state/reducers';

const middleware = [ thunk ];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
);

class MyApp extends App {
  render () {
    const { Component, pageProps } = this.props;

    return (
      <Provider store={store}>
        <Container>
          <Head>
            <title>Shopping List</title>
          </Head>
          <Component {...pageProps} />
        </Container>
      </Provider>
    );
  }
}

export default MyApp;

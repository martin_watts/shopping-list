import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

import gridCss from 'purecss/build/grids-min.css';
import responsiveGridCss from 'purecss/build/grids-responsive-min.css';

import {
  fetchShoppingList,
  addShoppingListItem,
  deleteShoppingListItem,
  setShoppingListItemMarked,
  sortShoppingListItem
} from '../state/actions/shopping-list';
import { getShoppingListItems, getIsFetching } from '../state/selectors/shopping-list';
import ShoppingList from '../components/shopping-list';
import ShoppingListInput from '../components/shopping-list-input';

import css from '../css/styles.css';

export class UnconnectedIndexPage extends React.Component {
  componentDidMount() {
    const { onLoad } = this.props;

    onLoad();
  }

  render() {
    const {
      items,
      isFetching,
      handleAddNewItem,
      handleDeleteItem,
      handleToggleItemMarked,
      handleSortItem
    } = this.props;

    return (
      <section id="shopping-list" className={[css['shopping-list-section'], gridCss['pure-g']].join(' ')}>
        <div
          className={[
            gridCss['pure-u-1'],
            responsiveGridCss['pure-u-md-20-24'],
            responsiveGridCss['pure-u-lg-18-24']
          ].join(' ')}
        >
          <ShoppingListInput onAddItem={handleAddNewItem} />
          <ShoppingList
            items={items}
            isFetching={isFetching}
            onDeleteItem={handleDeleteItem}
            onToggleItemMarked={handleToggleItemMarked}
            onSortItem={handleSortItem}
          />
        </div>
      </section>
    );
  }
}

UnconnectedIndexPage.propTypes = {
  items: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.number,
        marked: PropTypes.bool.isRequired
      })
    ),
    PropTypes.object // Error case
  ]).isRequired,
  isFetching: PropTypes.bool.isRequired,
  onLoad: PropTypes.func.isRequired,
  handleAddNewItem: PropTypes.func.isRequired,
  handleDeleteItem: PropTypes.func.isRequired,
  handleToggleItemMarked: PropTypes.func.isRequired,
  handleSortItem: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  items: getShoppingListItems(state),
  isFetching: getIsFetching(state)
});

const mapDispatchToProps = dispatch => ({
  onLoad: () => dispatch(fetchShoppingList()),
  handleAddNewItem: title => dispatch(addShoppingListItem(title)),
  handleDeleteItem: id => dispatch(deleteShoppingListItem(id)),
  handleToggleItemMarked: (id, marked) => dispatch(setShoppingListItemMarked(id, marked)),
  handleSortItem: (id, oldIndex, newIndex) => dispatch(sortShoppingListItem(id, oldIndex, newIndex))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UnconnectedIndexPage);

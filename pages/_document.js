import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';

import 'purecss/build/base-min.css';

import css from '../css/styles.css';

export default class MyDocument extends Document {
  render() {
    return (
      <html lang="en-GB">
        <Head>
         <meta name="viewport" content="width=device-width, initial-scale=1"/>
        </Head>
        <body className={css.body}>
          <header className={css['site-header']}>
            <h1>Shopping List</h1>
          </header>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

import React from 'react';
import PropTypes from 'prop-types';

import css from '../../css/styles.css';

export const ENTER_KEY = 13;

class ShoppingListInput extends React.Component {
  constructor(props) {
    super(props);

    this.initialState = {
      value: ''
    };
    this.state = this.initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  handleKeyDown(e) {
    if (e.which === ENTER_KEY) {
      const { onAddItem } = this.props;

      onAddItem(e.target.value);
      this.setState(this.initialState);
    }
  }

  render() {
    const {value} = this.state || {};

    return (
      <input
        className={css['shopping-list-input']}
        name="addListItem"
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        value={value}
      />
    );
  }
}

ShoppingListInput.propTypes = {
  onAddItem: PropTypes.func.isRequired
};

export default ShoppingListInput;

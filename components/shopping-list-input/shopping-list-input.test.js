import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import ShoppingListInput, { ENTER_KEY } from './shopping-list-input';

let mockHandleAddItem;

describe('The shopping list input component', () => {
  let input;

  beforeEach(() => {
    jest.clearAllMocks();

    mockHandleAddItem = jest.fn();

    input = mount(
      <ShoppingListInput
        onAddItem={mockHandleAddItem}
      />
    );
  });

  test('renders correctly', () => {
    expect(mountToJson(input, {mode: 'deep'})).toMatchSnapshot();
  });

  test('changes its value on input', () => {
    // Assign
    const newValue = 'Spam';
    const event = {target: {name: 'testInput', value: newValue}};

    // Act
    input.simulate('change', event);

    // Assert
    expect(input.find('input').instance().value).toBe(newValue);
  });

  test('submits its value on enter key down', () => {
    // Assign
    const value = 'Spam, spam, spam';
    const event = {which: ENTER_KEY, target: {name: 'testInput', value}};

    // Act
    input.simulate('keyDown', event);

    // Assert
    expect(input.find('input').instance().value).toBe('');
    expect(mockHandleAddItem).toHaveBeenCalledWith(value);
  });

  test('does not submit its value on non-enter key down', () => {
    // Assign
    const BACKSPACE_KEY = 8;
    const value = 'Spam, spam, spam, egg and spam';
    const event = {which: BACKSPACE_KEY, target: {name: 'testInput', value}};
    input.simulate('change', event); // set the value

    // Act
    input.simulate('keyDown', event);

    // Assert
    expect(input.find('input').instance().value).toBe(value);
    expect(mockHandleAddItem).not.toHaveBeenCalled();
  });
});

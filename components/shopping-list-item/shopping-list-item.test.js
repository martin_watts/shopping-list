import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import {BaseListItem as ShoppingListItem} from './shopping-list-item';

const unmarkedItem = {
  _id: '1',
  title: 'Apples',
  marked: false
};
const markedItem = {
  _id: '2',
  title: 'Cat food',
  marked: true
};
let mockOnDeleteItem;
let mockOnToggleItemMarked;

const renderComponent = (item = unmarkedItem) => {
  const wrapper = mount(
    <ShoppingListItem
      item={item}
      onDeleteItem={mockOnDeleteItem}
      onToggleItemMarked={mockOnToggleItemMarked}
    />
  );

  return wrapper;
};

describe('The shopping list item component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mockOnDeleteItem = jest.fn();
    mockOnToggleItemMarked = jest.fn();
  });

  test('renders correctly', () => {
    const wrapper = renderComponent();

    expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot();
  });

  test('calls its delete handler when the delete button is clicked', () => {
    const wrapper = renderComponent();
    const deleteButton = wrapper.find('button');

    // Assert - no false positives
    expect(mockOnDeleteItem).not.toHaveBeenCalled();

    // Act
    deleteButton.simulate('click');

    // Assert
    expect(mockOnDeleteItem).toHaveBeenCalled();
  });

  test('calls its toggle item marked handler when the checkbox value is changed', () => {
    const wrapper = renderComponent();
    const toggleInput = wrapper.find('input');

    // Assert - no false positives
    expect(mockOnToggleItemMarked).not.toHaveBeenCalled();

    // Act
    toggleInput.simulate('change');

    // Assert
    expect(mockOnToggleItemMarked).toHaveBeenCalled();
  });

  test('renders a marked item correctly', () => {
    const wrapper = renderComponent(markedItem);

    expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('marked_item');
  });
});

import PropTypes from 'prop-types';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGripHorizontal, faTrashAlt, faCheck } from '@fortawesome/free-solid-svg-icons';
import { sortableElement, sortableHandle } from 'react-sortable-hoc';

import buttonCss from 'purecss/build/buttons-min.css';
import css from '../../css/styles.css';

const DragHandle = sortableHandle(() => <FontAwesomeIcon className={css['drag-handle']} icon={faGripHorizontal}/>);


export const BaseListItem = ({ item, onDeleteItem, onToggleItemMarked }) => (
  <li className={[css['shopping-list-item'], item.marked ? css['marked-item'] : ''].join(' ')}>
    <DragHandle/>
    <input
      id={`toggle-${item._id}`}
      className={css['shopping-list-item-checkbox']}
      type="checkbox"
      checked={item.marked}
      onChange={onToggleItemMarked}
    />
    <label htmlFor={`toggle-${item._id}`}>
      <FontAwesomeIcon icon={faCheck}/>
    </label>
    <span>{item.title}</span>
    <button className={buttonCss['pure-button']}
      onClick={onDeleteItem}
    >
      <FontAwesomeIcon icon={faTrashAlt}/>
    </button>
  </li>
);

BaseListItem.propTypes = {
  item: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    marked: PropTypes.bool.isRequired
  }).isRequired,
  onDeleteItem: PropTypes.func.isRequired,
  onToggleItemMarked: PropTypes.func.isRequired
};

export default sortableElement(BaseListItem);

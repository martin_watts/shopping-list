import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import ShoppingList from './shopping-list';

const mockOnDeleteItem = jest.fn();
const mockOnToggleItemMarked = jest.fn();
const mockOnSortItem = jest.fn();

const renderComponent = (items = [], isFetching = false) => {
  const wrapper = mount(
    <ShoppingList
      items={items}
      isFetching={isFetching}
      onDeleteItem={mockOnDeleteItem}
      onToggleItemMarked={mockOnToggleItemMarked}
      onSortItem={mockOnSortItem}
    />
  );

  return wrapper;
};

describe('The shopping list component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('renders correctly with no items', () => {
    const wrapper = renderComponent();

    expect(mountToJson(wrapper, { mode: 'deep' })).toMatchSnapshot('no_items');
  });

  test('renders correctly with items', () => {
    const mockItems = [
      {
        _id: '1',
        title: 'Apples',
        marked: false
      },
      {
        _id: '2',
        title: 'Oranges',
        marked: false
      }
    ];

    const wrapper = renderComponent(mockItems);

    expect(mountToJson(wrapper, { mode: 'deep' })).toMatchSnapshot('items');
  });

  test('renders correctly when loading items', () => {
    const mockItems = [];
    const mockIsFetching = true;

    const wrapper = renderComponent(mockItems, mockIsFetching);

    expect(mountToJson(wrapper, { mode: 'deep' })).toMatchSnapshot('is_fetching');
  });


  test('renders correctly when there was a problem loading the items', () => {
    const mockItems = new Error('Load failed');

    const wrapper = renderComponent(mockItems);

    expect(mountToJson(wrapper, { mode: 'deep' })).toMatchSnapshot('load_error');
  });

  test('calls its delete handler with the deleted item id when the list item is deleted', () => {
    const mockItems = [
      {
        _id: '3',
        title: 'Carrots',
        marked: false
      },
      {
        _id: '4',
        title: 'Cabbages',
        marked: false
      }
    ];

    const wrapper = renderComponent(mockItems);
    const item1 = wrapper.find('ul').children().first();
    const item2 = wrapper.find('ul').children().last();

    // Assert - no false positives
    expect(mockOnDeleteItem).not.toHaveBeenCalled();

    // Act
    item1.props().onDeleteItem();

    // Assert
    expect(mockOnDeleteItem).toHaveBeenCalledWith(mockItems[0]._id);

    mockOnDeleteItem.mockReset();

    // Act
    item2.props().onDeleteItem();

    // Assert
    expect(mockOnDeleteItem).toHaveBeenCalledWith(mockItems[1]._id);
  });

  test('calls its toggle item marked handler with the toggled items id and new marked state when the list item is toggled', () => {
    const mockItems = [
      {
        _id: '5',
        title: 'Tic-tacs',
        marked: false
      },
      {
        _id: '6',
        title: 'Spaghetti hoops',
        marked: true
      }
    ];

    const wrapper = renderComponent(mockItems);
    const item1 = wrapper.find('ul').children().first();
    const item2 = wrapper.find('ul').children().last();


    // Assert - no false positives
    expect(mockOnToggleItemMarked).not.toHaveBeenCalled();

    // Act
    item1.props().onToggleItemMarked();

    // Assert
    expect(mockOnToggleItemMarked).toHaveBeenCalledWith(mockItems[0]._id, !mockItems[0].marked);

    mockOnToggleItemMarked.mockReset();

    // Act
    item2.props().onToggleItemMarked();

    // Assert
    expect(mockOnToggleItemMarked).toHaveBeenCalledWith(mockItems[1]._id, !mockItems[1].marked);
  });

  test('calls its sort item handler the moved items id, old index and new index when it is sorted', () => {
    const mockItems = [
      {
        _id: '7',
        title: 'Allsorts',
        marked: false
      },
      {
        _id: '8',
        title: 'Wine gums',
        marked: true
      }
    ];
    const oldIndex = 0;
    const newIndex = 1;

    const wrapper = renderComponent(mockItems);
    const sortableList = wrapper.childAt(0).childAt(0);

    // Assert - no false positives
    expect(mockOnSortItem).not.toHaveBeenCalled();

    // Act
    sortableList.props().onSortEnd({oldIndex, newIndex});

    // Assert
    expect(mockOnSortItem).toHaveBeenCalledWith(mockItems[0]._id, oldIndex, newIndex);

    mockOnSortItem.mockReset();

    // Act
    sortableList.props().onSortEnd({oldIndex: newIndex, newIndex: oldIndex});

    // Assert
    expect(mockOnSortItem).toHaveBeenCalledWith(mockItems[1]._id, newIndex, oldIndex);
  });
});

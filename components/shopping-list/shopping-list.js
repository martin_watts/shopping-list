import PropTypes from 'prop-types';
import React from 'react';
import {sortableContainer} from 'react-sortable-hoc';

import ShoppingListItem from '../shopping-list-item';

import css from '../../css/styles.css';


const SortableShoppingList = sortableContainer(({items, onDeleteItem, onToggleItemMarked}) => {
  return (
    <ul className={css['shopping-list']}>
      {items.map((item, index) => (
        <ShoppingListItem
          key={item._id}
          index={index}
          item={item}
          onDeleteItem={() => onDeleteItem(item._id)}
          onToggleItemMarked={() => onToggleItemMarked(item._id, !item.marked)}
        />
      ))}
    </ul>
  );
});

class ShoppingList extends React.Component {
  constructor(props) {
    super(props);
    this.handleSortEnd = this.handleSortEnd.bind(this);
  }

  handleSortEnd({oldIndex, newIndex}) {
    const {items, onSortItem} = this.props;
    const item = items[oldIndex];

    onSortItem(item._id, oldIndex, newIndex);
  }

  render() {
    const { items, isFetching} = this.props;

    if (isFetching) { // TODO: Consider putting the spinner in to its own component
      return (
        <div className={css.spinner}>
          <div className={css.bounce1}/>
          <div className={css.bounce2}/>
          <div className={css.bounce3}/>
        </div>
      );
    }

    const isError = items instanceof Error;

    if (isError) { // TODO: Handle this is a better way (auto-retry first/nicer error message based on the error)
      return (
        <span>
          The shopping list didn't load :(. You could try refreshing the page.
        </span>
      );
    }

    if (Array.isArray(items)) {
      return (
        <SortableShoppingList
          {...this.props}
          onSortEnd={this.handleSortEnd}
          useWindowAsScrollContainer
          useDragHandle
        />
      );
    }

    return '';
  }
}

ShoppingList.propTypes = {
  items: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.number,
        marked: PropTypes.bool.isRequired
      })
    ),
    PropTypes.object // Error case
  ]).isRequired,
  isFetching: PropTypes.bool.isRequired,
  onDeleteItem: PropTypes.func.isRequired,
  onToggleItemMarked: PropTypes.func.isRequired,
  onSortItem: PropTypes.func.isRequired
};

export default ShoppingList;

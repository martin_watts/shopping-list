import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import React from 'react';

import { UnconnectedIndexPage } from '../pages/index';

describe('The index page', () => {
  let mockOnLoad;
  let mockHandleAddNewItem;
  let mockHandleDeleteItem;
  let mockHandleToggleItemMarked;
  let mockHandleSortItem;

  beforeEach(() => {
    jest.clearAllMocks();

    mockOnLoad = jest.fn();
    mockHandleAddNewItem = jest.fn();
    mockHandleDeleteItem = jest.fn();
    mockHandleToggleItemMarked = jest.fn();
    mockHandleSortItem = jest.fn();
  });

  const renderPage = (items = [], isFetching = false) => {
    const wrapper = shallow(
      <UnconnectedIndexPage
        items={items}
        isFetching={isFetching}
        onLoad={mockOnLoad}
        handleAddNewItem={mockHandleAddNewItem}
        handleDeleteItem={mockHandleDeleteItem}
        handleToggleItemMarked={mockHandleToggleItemMarked}
        handleSortItem={mockHandleSortItem}
      />
    );

    return wrapper;
  };

  test('renders correctly', () => {
    // Assign
    const indexPage = renderPage();
    // Assert
    expect(shallowToJson(indexPage)).toMatchSnapshot();
  });

  test('calls a provided onLoad handler when mounted', () => {
    // Assign
    renderPage();

    // Assert
    expect(mockOnLoad).toHaveBeenCalled();
  });

  test('passes its props correctly', () => {
    // Assign
    const mockItems = [{
      _id: '5',
      title: 'Butter',
      marked: false
    }];

    const mockIsFetching = true;
    const indexPage = renderPage(mockItems, mockIsFetching);

    // Assert
    expect(shallowToJson(indexPage)).toMatchSnapshot('props');
  });
});

import {getShoppingListItems, getIsFetching} from './shopping-list';

describe('shopping list selector', () => {
  describe('getShoppingListItems', () => {
    test('selects the items from state', () => {
      const expectedItems = [{
        _id: '5',
        title: 'Beetroot'
      }];

      expect(getShoppingListItems({items: expectedItems})).toBe(expectedItems);
    });

    test('copes with state being falsy', () => {
      expect(getShoppingListItems(null)).toBeNull();
    });
  });

  describe('getIsFetching', () => {
    test('selects the fetching flag from state', () => {
      let expectedStatus = true;

      expect(getIsFetching({isFetching: expectedStatus})).toBe(expectedStatus);

      expectedStatus = false;

      expect(getIsFetching({isFetching: expectedStatus})).toBe(expectedStatus);
    });

    test('copes with state being falsy', () => {
      expect(getIsFetching(null)).toBeNull();
    });
  });
});

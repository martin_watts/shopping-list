export const getShoppingListItems = state => (state && state.items);

export const getIsFetching = state => (state && state.isFetching);

import shoppingList from './shopping-list';
import {
  SHOPPING_LIST_REQUEST,
  SHOPPING_LIST_RECEIVED,
  SHOPPING_LIST_REQUEST_FAIL,
  ADD_SHOPPING_LIST_ITEM,
  DELETE_SHOPPING_LIST_ITEM,
  MARK_SHOPPING_LIST_ITEM,
  UNMARK_SHOPPING_LIST_ITEM,
  SORT_SHOPPING_LIST_ITEM,
  SORT_SHOPPING_LIST_ITEM_FAIL
} from '../actions/shopping-list';

describe('shopping list reducers', () => {
  const initialState = {
    isFetching: false,
    items: []
  };

  it('should provide the initial state', () => {
    expect(shoppingList(undefined, {})).toEqual(initialState);
  });

  it('should handle the SHOPPING_LIST_REQUEST action', () => {
    expect(shoppingList(initialState, { type: SHOPPING_LIST_REQUEST })).toEqual({
      ...initialState,
      isFetching: true
    });
  });

  it('should handle the SHOPPING_LIST_RECEIVED action', () => {
    const newItems = [
      {
        _id: '4',
        title: 'Chicken',
        marked: false
      }
    ];

    expect(
      shoppingList({ ...initialState, isFetching: true }, { type: SHOPPING_LIST_RECEIVED, items: newItems })
    ).toEqual({
      ...initialState,
      items: newItems
    });
  });

  it('should handle the SHOPPING_LIST_REQUEST_FAIL action', () => {
    const err = new Error('Expected error');

    expect(shoppingList({ ...initialState, isFetching: true }, { type: SHOPPING_LIST_REQUEST_FAIL, err })).toEqual({
      ...initialState,
      items: err
    });
  });

  it('should handle the ADD_SHOPPING_LIST_ITEM action', () => {
    const newItem = {
      _id: '123',
      title: 'Coffee',
      marked: false
    };

    expect(shoppingList(initialState, { type: ADD_SHOPPING_LIST_ITEM, item: newItem })).toEqual({
      ...initialState,
      items: [
        newItem
      ]
    });
  });

  it('should handle the DELETE_SHOPPING_LIST_ITEM action', () => {
    const existingItems = [
      {
        _id: '7',
        title: 'Cake',
        marked: true
      },
      {
        _id: '8',
        title: 'Ice cream',
        marked: false
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        { type: DELETE_SHOPPING_LIST_ITEM, _id: existingItems[0]._id }
      )
    ).toEqual({
      ...initialState,
      items: [existingItems[1]]
    });
  });

  it('should handle the MARK_SHOPPING_LIST_ITEM action', () => {
    const existingItems = [
      {
        _id: '9',
        title: 'Rice',
        marked: false
      },
      {
        _id: '10',
        title: 'Pasta',
        marked: true
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        { type: MARK_SHOPPING_LIST_ITEM, _id: existingItems[0]._id }
      )
    ).toEqual({
      ...initialState,
      items: [
        {
          ...existingItems[0],
          marked: true
        },
        existingItems[1]
      ]
    });
  });

  it('should handle the UNMARK_SHOPPING_LIST_ITEM action', () => {
    const existingItems = [
      {
        _id: '11',
        title: 'Jam',
        marked: true
      },
      {
        _id: '12',
        title: 'Marmalade',
        marked: true
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        { type: UNMARK_SHOPPING_LIST_ITEM, _id: existingItems[1]._id }
      )
    ).toEqual({
      ...initialState,
      items: [
        existingItems[0],
        {
          ...existingItems[1],
          marked: false
        }
      ]
    });
  });

  it('should handle the SORT_SHOPPING_LIST_ITEM action', () => {
    const existingItems = [
      {
        _id: '12',
        title: 'Salt',
        marked: false
      },
      {
        _id: '13',
        title: 'Sugar',
        marked: false
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        { type: SORT_SHOPPING_LIST_ITEM, _id: existingItems[1]._id, oldIndex: 1, newIndex: 0 }
      )
    ).toEqual({
      ...initialState,
      items: [
        existingItems[1],
        existingItems[0]
      ]
    });
  });

  it('should handle the SORT_SHOPPING_LIST_ITEM_FAIL action if the item has not moved again', () => {
    const existingItems = [
      {
        _id: '14',
        title: 'Pancakes',
        marked: false
      },
      {
        _id: '13',
        title: 'Scones',
        marked: false
      },
      {
        _id: '15',
        title: 'Bread',
        marked: false
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        { type: SORT_SHOPPING_LIST_ITEM_FAIL, _id: existingItems[0]._id, oldIndex: 1, newIndex: 0 }
        )
        ).toEqual({
      ...initialState,
      items: [
        existingItems[1],
        existingItems[0],
        // eslint-disable-next-line no-magic-numbers
        existingItems[2]
      ]
    });
  });

  it('should not re-sort the items on SORT_SHOPPING_LIST_ITEM_FAIL, if the item has moved again', () => {
    const existingItems = [
      {
        _id: '13',
        title: 'Scones',
        marked: false
      },
      {
        _id: '15',
        title: 'Bread',
        marked: false
      },
      {
        _id: '14',
        title: 'Pancakes',
        marked: false
      }
    ];

    expect(
      shoppingList(
        { ...initialState, items: existingItems },
        // eslint-disable-next-line no-magic-numbers
        { type: SORT_SHOPPING_LIST_ITEM_FAIL, _id: existingItems[2]._id, oldIndex: 1, newIndex: 0 }
        )
        ).toEqual({
      ...initialState,
      items: existingItems
    });
  });
});

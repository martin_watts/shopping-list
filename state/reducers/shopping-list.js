import arrayMove from 'array-move';

import {
  SHOPPING_LIST_REQUEST,
  SHOPPING_LIST_REQUEST_FAIL,
  SHOPPING_LIST_RECEIVED,
  ADD_SHOPPING_LIST_ITEM,
  ADD_SHOPPING_LIST_ITEM_FAIL,
  DELETE_SHOPPING_LIST_ITEM,
  MARK_SHOPPING_LIST_ITEM,
  MARK_SHOPPING_LIST_ITEM_FAIL,
  UNMARK_SHOPPING_LIST_ITEM,
  UNMARK_SHOPPING_LIST_ITEM_FAIL,
  SORT_SHOPPING_LIST_ITEM,
  SORT_SHOPPING_LIST_ITEM_FAIL
} from '../actions/shopping-list';


const reducerMap = {};

reducerMap[SHOPPING_LIST_REQUEST] = (state) => {
  return {
    ...state,
    isFetching: true
  };
};

reducerMap[SHOPPING_LIST_RECEIVED] = (state, action) => {
  return {
    ...state,
    isFetching: false,
    items: action.items
  };
};

reducerMap[SHOPPING_LIST_REQUEST_FAIL] = (state, action) => {
  return {
    ...state,
    isFetching: false,
    items: action.err
  };
};

reducerMap[ADD_SHOPPING_LIST_ITEM] = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  return {
    ...state,
    items: [
      ...items,
      action.item
    ]
  };
};

const removeItem = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  return {
    ...state,
    items: [
      ...items.filter(item => item._id !== action._id)
    ]
  };
};
reducerMap[DELETE_SHOPPING_LIST_ITEM] = removeItem;
reducerMap[ADD_SHOPPING_LIST_ITEM_FAIL] = removeItem;

// TODO: Handle DELETE_SHOPPING_LIST_ITEM_FAIL

const markItem = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  return {
    ...state,
    items: items.map(item => item._id !== action._id ? item : {
        ...item,
        marked: true
      }
    )
  };
};
reducerMap[MARK_SHOPPING_LIST_ITEM] = markItem;
reducerMap[UNMARK_SHOPPING_LIST_ITEM_FAIL] = markItem;

const unmarkItem = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  return {
    ...state,
    items: items.map(item => item._id !== action._id ? item : {
        ...item,
        marked: false
      }
    )
  };
};
reducerMap[UNMARK_SHOPPING_LIST_ITEM] = unmarkItem;
reducerMap[MARK_SHOPPING_LIST_ITEM_FAIL] = unmarkItem;

reducerMap[SORT_SHOPPING_LIST_ITEM] = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  return {
    ...state,
    items: arrayMove(items, action.oldIndex, action.newIndex)
  };
};

reducerMap[SORT_SHOPPING_LIST_ITEM_FAIL] = (state, action) => {
  const items = (Array.isArray(state.items) ? state.items : []);
  const currItemAtNewIndex = items[action.newIndex];

  if (currItemAtNewIndex && currItemAtNewIndex._id === action._id) {
    return {
      ...state,
      items: arrayMove(items, action.newIndex, action.oldIndex)
    };
  } else {
    return state;
  }
};

const shoppingList = (
  state = {
    isFetching: false,
    items: []
  },
  action
) => {
  const reducer = reducerMap[action.type];

  if (reducer) {
    return reducer(state, action);
  }

  return state;
};

export default shoppingList;

/* global fetchMock */
import status from 'http-status';

import { SHOPPING_LIST_ENDPOINT } from '../../server/routes';

import api, { JSON_CONTENT_TYPE } from './shopping-list-api';

describe('The shopping-list client API', () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  afterAll(() => {
    fetchMock.resetMocks();
  });

  describe('getting items', () => {
    const mockItems = [{
      _id: '1',
      title: 'Cheese',
      marked: true
    }, {
      _id: '2',
      title: 'Crackers',
      marked: false
    }];

    test('succeeds if the return code is 200', done => {
      // Assign
      fetchMock.mockResponseOnce(JSON.stringify(mockItems), { status: status.OK });

      // Act
      api.getItems()
        .then(items => {
          const lastFetchArgs = fetchMock.mock.calls[fetchMock.mock.calls.length - 1];

          expect(lastFetchArgs).toEqual([SHOPPING_LIST_ENDPOINT]);

          expect(items).toEqual(mockItems);
          done();
        });
    });

    test('it throws an error if the return code is not 200', done => {
      // Assign
      fetchMock.mockResponseOnce('Internal server error', { status: status.INTERNAL_SERVER_ERROR });

      // Act
      api.getItems()
        .then(() => {
          done.fail(new Error('get items succeeded when the GET request failed'));
        })
        .catch(err => {
          expect(err).toBeDefined();
          done();
        });
    });
  });

  describe('adding a new item', () => {
    const newItem = {
      _id: '3',
      title: 'Wine',
      marked: false
    };

    test('succeeds if the return code is 201', done => {
      // Assign
      fetchMock.mockResponseOnce(JSON.stringify({message: 'Created item', insertedId: '123'}), { status: status.CREATED });

      // Act
      api.addItem(newItem)
        .then(() => {
          const lastFetchArgs = fetchMock.mock.calls[fetchMock.mock.calls.length - 1];

          expect(lastFetchArgs).toEqual([
            SHOPPING_LIST_ENDPOINT,
            {
              method: 'POST',
              headers: JSON_CONTENT_TYPE,
              body: JSON.stringify(newItem)
            }
          ]);

          done();
        });
    });

    test('it throws an error if the return code is not 201', done => {
      // Assign
      fetchMock.mockResponseOnce('Internal server error', { status: status.INTERNAL_SERVER_ERROR });

      // Act
      api.addItem(newItem)
        .then(() => {
          done.fail(new Error('add item succeeded when the POST request failed'));
        })
        .catch(err => {
          expect(err).toBeDefined();
          done();
        });
    });
  });

  describe('deleting an item', () => {
    const id = '4';

    test('succeeds if the return code is 204', done => {
      // Assign
      fetchMock.mockResponseOnce(null, { status: status.NO_CONTENT });

      // Act
      api.deleteItem(id)
        .then(() => {
          const lastFetchArgs = fetchMock.mock.calls[fetchMock.mock.calls.length - 1];

          expect(lastFetchArgs).toEqual([
            `${SHOPPING_LIST_ENDPOINT}/${id}`,
            {
              method: 'DELETE'
            }
          ]);

          done();
        });
    });

    test('it throws an error if the return code is not 204', done => {
      // Assign
      fetchMock.mockResponseOnce('Internal server error', { status: status.INTERNAL_SERVER_ERROR });

      // Act
      api.deleteItem(id)
        .then(() => {
          done.fail(new Error('delete item succeeded when the DELETE request failed'));
        })
        .catch(err => {
          expect(err).toBeDefined();
          done();
        });
    });
  });

  describe('updating an item', () => {
    const id = '5';
    const changeset = { marked: true };

    test('succeeds if the return code is 200', done => {
      // Assign
      fetchMock.mockResponseOnce(JSON.stringify({message: 'Updated item', changeset}), { status: status.OK });

      // Act
      api.updateItem(id, changeset)
        .then(() => {
          const lastFetchArgs = fetchMock.mock.calls[fetchMock.mock.calls.length - 1];

          expect(lastFetchArgs).toEqual([
            `${SHOPPING_LIST_ENDPOINT}/${id}`,
            {
              method: 'PATCH',
              headers: JSON_CONTENT_TYPE,
              body: JSON.stringify(changeset)
            }
          ]);

          done();
        });
    });

    test('it throws an error if the return code is not 200', done => {
      // Assign
      fetchMock.mockResponseOnce('Internal server error', { status: status.INTERNAL_SERVER_ERROR });

      // Act
      api.updateItem(id, changeset)
        .then(() => {
          done.fail(new Error('update item succeeded when the PATCH request failed'));
        })
        .catch(err => {
          expect(err).toBeDefined();
          done();
        });
    });
  });
});

import fetch from 'isomorphic-unfetch';
import status from 'http-status';

import { SHOPPING_LIST_ENDPOINT } from '../../server/routes';

export const JSON_CONTENT_TYPE = {
  'Content-Type': 'application/json'
};

export default {
  getItems: () => {
    return fetch(SHOPPING_LIST_ENDPOINT)
      .then(result => {
        if (result.status !== status.OK) {
          throw new Error('Fetching the list failed');
        }

        return result.json();
      });
  },
  addItem: (newItem) => {
    return fetch(SHOPPING_LIST_ENDPOINT, {
      method: 'POST',
      headers: JSON_CONTENT_TYPE,
      body: JSON.stringify(newItem)
    })
    .then(result => {
      if (result.status !== status.CREATED) {
        throw new Error('Creating the item failed');
      }

      return result.json();
    });
  },
  deleteItem: (id) => {
    return fetch(`${SHOPPING_LIST_ENDPOINT}/${id}`, {
      method: 'DELETE'
    })
    .then(result => {
      if (result.status !== status.NO_CONTENT) {
        throw new Error('Deleting the item failed');
      }
    });
  },
  updateItem: (id, changeset) => {
    return fetch(`${SHOPPING_LIST_ENDPOINT}/${id}`, {
      method: 'PATCH',
      headers: JSON_CONTENT_TYPE,
      body: JSON.stringify(changeset)
    })
    .then(result => {
      if (result.status !== status.OK) {
        throw new Error('Updating the item failed');
      }

      return result.json();
    });
  }
};

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import api from '../services/shopping-list-api';

import {
  SHOPPING_LIST_REQUEST,
  SHOPPING_LIST_RECEIVED,
  fetchShoppingList,
  ADD_SHOPPING_LIST_ITEM,
  addShoppingListItem,
  DELETE_SHOPPING_LIST_ITEM,
  deleteShoppingListItem,
  MARK_SHOPPING_LIST_ITEM,
  UNMARK_SHOPPING_LIST_ITEM,
  setShoppingListItemMarked,
  SORT_SHOPPING_LIST_ITEM,
  sortShoppingListItem
} from './shopping-list';


const middleware = [thunk];
const mockStore = configureMockStore(middleware);

beforeEach(() => {
  jest.clearAllMocks();
});

describe('when not already loading', () => {
  test('fetching the shopping list should dispatch a request, followed by a success action', done => {
    // Assign
    const store = mockStore({
      isFetching: false,
      items: []
    });

    jest.spyOn(api, 'getItems').mockReturnValue(Promise.resolve([]));

    // Act
    store.dispatch(fetchShoppingList()).then(() => {
      // Assert
      expect(store.getActions()).toEqual([
        { type: SHOPPING_LIST_REQUEST },
        {
          type: SHOPPING_LIST_RECEIVED,
          items: []
        }
      ]);

      done();
    });
  });
});

describe('when already loading', () => {
  test('fetching the shopping list should not dispatch a new request action', done => {
     // Assign
     const store = mockStore({
      isFetching: true,
      items: []
    });

    // Act
    store.dispatch(fetchShoppingList()).then(() => {
      // Assert
      expect(store.getActions()).toEqual([]);

      done();
    });
  });
});

describe('when adding new items', () => {
  test('should dispatch a new add action', done => {
     // Assign
    const store = mockStore();
    const itemTitle = 'Bananas';

    jest.spyOn(api, 'addItem').mockReturnValue(Promise.resolve({message: 'Inserted item', insertedId: '123'}));

    // Act
    store.dispatch(addShoppingListItem(itemTitle)).then(() => {
      // Assert
      expect(store.getActions()).toEqual([{
        type: ADD_SHOPPING_LIST_ITEM,
        item: {
          _id: expect.any(String),
          title: itemTitle,
          marked: false
        }
      }]);

      done();
    });
  });
});

describe('when deleting existing items', () => {
  test('should dispatch a new delete action', done => {
     // Assign
    const store = mockStore();
    const itemId = '6';

    jest.spyOn(api, 'deleteItem').mockReturnValue(Promise.resolve());

    // Act
    store.dispatch(deleteShoppingListItem(itemId)).then(() => {
      // Assert
      expect(store.getActions()).toEqual([{
        type: DELETE_SHOPPING_LIST_ITEM,
        _id: itemId
      }]);

      done();
    });
  });
});

describe('when toggling an items marked state', () => {
  test(`should dispatch a new 'mark' action if the new state is marked`, done => {
     // Assign
    const store = mockStore();
    const itemId = '7';
    const marked = true;

    jest.spyOn(api, 'updateItem').mockReturnValue(Promise.resolve({message: 'Updated item', changeset: { marked: true }}));

    // Act
    store.dispatch(setShoppingListItemMarked(itemId, marked)).then(() => {
      // Assert
      expect(store.getActions()).toEqual([{
        type: MARK_SHOPPING_LIST_ITEM,
        _id: itemId
      }]);

      done();
    });
  });

  test(`should dispatch a new 'unmark' action if the new state is not marked`, done => {
    // Assign
   const store = mockStore();
   const itemId = '8';
   const marked = false;

   jest.spyOn(api, 'updateItem').mockReturnValue(Promise.resolve({message: 'Updated item', changeset: { marked: false }}));

   // Act
   store.dispatch(setShoppingListItemMarked(itemId, marked)).then(() => {
     // Assert
     expect(store.getActions()).toEqual([{
       type: UNMARK_SHOPPING_LIST_ITEM,
       _id: itemId
     }]);

     done();
   });
 });
});

describe('when re-sorting items', () => {
  test(`should dispatch a new 'sort' action`, done => {
     // Assign
    const store = mockStore();
    const itemId = '8';
    const oldIndex = 2;
    const newIndex = 4;

    jest.spyOn(api, 'updateItem').mockReturnValue(Promise.resolve({message: 'Updated item', changeset: { rank: newIndex }}));

    // Act
    store.dispatch(sortShoppingListItem(itemId, oldIndex, newIndex)).then(() => {
      // Assert
      expect(store.getActions()).toEqual([{
        type: SORT_SHOPPING_LIST_ITEM,
        _id: itemId,
        oldIndex,
        newIndex
      }]);

      done();
    });
  });
});

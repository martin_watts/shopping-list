import ObjectId from 'bson-objectid';

import api from '../services/shopping-list-api';

const getNewListItem = title => ({
  _id: new ObjectId().toString(),
  title,
  marked: false
});

export const SHOPPING_LIST_REQUEST = 'SHOPPING_LIST_REQUEST';
const requestList = () => ({
  type: SHOPPING_LIST_REQUEST
});

export const SHOPPING_LIST_RECEIVED = 'SHOPPING_LIST_RECEIVED';
const receiveList = items => ({
  type: SHOPPING_LIST_RECEIVED,
  items
});

export const SHOPPING_LIST_REQUEST_FAIL = 'SHOPPING_LIST_REQUEST_FAIL';
const requestListFailure = err => ({
  type: SHOPPING_LIST_REQUEST_FAIL,
  error: err
});

const fetchList = () => dispatch => {
  dispatch(requestList());

  return api
    .getItems()
    .then(items => {
      return dispatch(receiveList(items));
    })
    .catch(err => {
      let error = err;

      if (!err instanceof Error) {
        error = new Error('Fetching items failed');
      }

      return dispatch(requestListFailure(error));
    });
};

const shouldFetchList = state => {
  return !(state && state.isFetching);
};

export const fetchShoppingList = () => {
  return (dispatch, getState) => {
    if (shouldFetchList(getState())) {
      return dispatch(fetchList());
    } else {
      return Promise.resolve();
    }
  };
};

export const ADD_SHOPPING_LIST_ITEM = 'ADD_SHOPPING_LIST_ITEM';
const addItem = newItem => {
  return {
    type: ADD_SHOPPING_LIST_ITEM,
    item: newItem
  };
};

export const ADD_SHOPPING_LIST_ITEM_FAIL = 'ADD_SHOPPING_LIST_ITEM_FAIL';
const addItemFail = newItem => {
  return {
    type: ADD_SHOPPING_LIST_ITEM_FAIL,
    _id: newItem._id
  };
};

export const addShoppingListItem = title => {
  return dispatch => {
    const newItem = getNewListItem(title);

    // Optimistic state update
    dispatch(addItem(newItem));

    return api.addItem(newItem).catch(() => {
      // Allow recovery on failure
      dispatch(addItemFail(newItem));
    });
  };
};

export const DELETE_SHOPPING_LIST_ITEM = 'DELETE_SHOPPING_LIST_ITEM';
const deleteItem = id => {
  return {
    type: DELETE_SHOPPING_LIST_ITEM,
    _id: id
  };
};

export const DELETE_SHOPPING_LIST_ITEM_FAIL = 'DELETE_SHOPPING_LIST_ITEM_FAIL';
const deleteItemFail = id => {
  return {
    type: DELETE_SHOPPING_LIST_ITEM_FAIL,
    _id: id
  };
};

export const deleteShoppingListItem = id => {
  return dispatch => {
    // Pessimistic update on delete to avoid having to re-fetch to recover on failure
    return api
      .deleteItem(id)
      .then(() => {
        return dispatch(deleteItem(id));
      })
      .catch(() => {
        //Allow for explanation
        return dispatch(deleteItemFail(id));
      });
  };
};

export const MARK_SHOPPING_LIST_ITEM = 'MARK_SHOPPING_LIST_ITEM';
export const UNMARK_SHOPPING_LIST_ITEM = 'UNMARK_SHOPPING_LIST_ITEM';
const toggleMarked = (id, marked) => {
  return {
    type: marked ? MARK_SHOPPING_LIST_ITEM : UNMARK_SHOPPING_LIST_ITEM,
    _id: id
  };
};

export const MARK_SHOPPING_LIST_ITEM_FAIL = 'MARK_SHOPPING_LIST_ITEM_FAIL';
export const UNMARK_SHOPPING_LIST_ITEM_FAIL = 'UNMARK_SHOPPING_LIST_ITEM_FAIL';
const toggleMarkedFail = (id, marked) => {
  return {
    type: marked ? MARK_SHOPPING_LIST_ITEM_FAIL : UNMARK_SHOPPING_LIST_ITEM_FAIL,
    _id: id
  };
};

export const setShoppingListItemMarked = (id, marked) => {
  return dispatch => {
    dispatch(toggleMarked(id, marked));

    return api.updateItem(id, { marked }).catch(() => {
      dispatch(toggleMarkedFail(id, marked));
    });
  };
};

export const SORT_SHOPPING_LIST_ITEM = 'SORT_SHOPPING_LIST_ITEM';
const sortItem = (id, oldIndex, newIndex) => {
  return {
    type: SORT_SHOPPING_LIST_ITEM,
    _id: id,
    oldIndex,
    newIndex
  };
};

export const SORT_SHOPPING_LIST_ITEM_FAIL = 'SORT_SHOPPING_LIST_ITEM_FAIL';
const sortItemFail = (id, oldIndex, newIndex) => {
  return {
    type: SORT_SHOPPING_LIST_ITEM_FAIL,
    _id: id,
    oldIndex,
    newIndex
  };
};

export const sortShoppingListItem = (id, oldIndex, newIndex) => {
  return dispatch => {
    dispatch(sortItem(id, oldIndex, newIndex));

    return api.updateItem(id, { rank: newIndex }).catch(() => {
      dispatch(sortItemFail(id, oldIndex, newIndex));
    });
  };
};

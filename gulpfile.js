/* eslint-env node */

const { series, src, dest, parallel } = require('gulp');
const babel = require('gulp-babel');
const env = require('gulp-env');
const exec = require('child_process').exec;
const fs = require('fs-extra');
const merge = require('merge-stream');
const path = require('path');

const distDir = path.resolve(__dirname, './dist');
const nextDir = path.resolve(__dirname, './.next');

const paths = {
  server: [
    {
      src: 'app.js',
      dest: distDir
    },
    {
      src: ['server/**/*.js'],
      dest: path.resolve(distDir, 'server')
    }
  ],
  prodArtefacts: [
    {
      src: nextDir,
      dest: path.resolve(distDir, '.next')
    },
    {
      src: path.resolve(__dirname, 'package.json'),
      dest: path.resolve(distDir, 'package.json')
    }
  ]
};

const handleExec = (cb, err, stdout, stderr) => {
  if (err) {
    console.error(err);
  }

  console.log(stdout);
  console.error(stderr);
  cb();
};

const clean = async () => {
  await fs.ensureDir(distDir);
  await fs.emptyDir(distDir);
  await fs.ensureDir('.next');
  await fs.emptyDir('.next');
};
clean.displayName = 'Cleaning .next and dist folders';

const buildNext = cb => {
  exec('NODE_ENV=production npx next build', (err, stdout, stderr) => handleExec(cb, err, stdout, stderr));
};
buildNext.displayName = 'Building Next.js project';

const buildServer = () => {
  const babelStreams = paths.server.map(serverPath => {
    const envs = env.set({
      NODE_ENV: 'production'
    });

    return src(serverPath.src, {
        ignore: '**/*.test.js'
      })
      .pipe(envs)
      .pipe(babel())
      .pipe(dest(serverPath.dest));
  });

  return merge(babelStreams);
};
buildServer.displayName = 'Building server';

const copyProdArtefacts = () => {
  const copyOperations = paths.prodArtefacts.map(artefactPath => fs.copy(artefactPath.src, artefactPath.dest));

  return Promise.all(copyOperations);
};
copyProdArtefacts.displayName = 'Copying build output to the dist folder';

const installProdNodeModules = (cb) => {
  exec('npm install --production', {cwd: distDir}, (err, stdout, stderr) => handleExec(cb, err, stdout, stderr));
};
installProdNodeModules.displayName = 'Installing production node modules';

const build = series(clean, parallel(buildNext, buildServer), copyProdArtefacts, installProdNodeModules);

exports.clean = clean;
exports.buildNext = buildNext;
exports.buildServer = buildServer;
exports.build = build;
exports.default = build;

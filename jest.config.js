module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  moduleNameMapper: {
    '(.+)\.css': 'identity-obj-proxy',
    'isomorphic-unfetch': '<rootDir>/__mocks__/isomorphic-unfetch',
    '(.+)\/db-client': '<rootDir>/__mocks__/db-client'
  },
  snapshotSerializers: ['enzyme-to-json/serializer']
};

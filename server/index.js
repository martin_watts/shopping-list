import compression from 'compression';
import express from 'express';
import next from 'next';

import { initDb } from './db-client';
import { SHOPPING_LIST_ENDPOINT } from './routes';
import shoppingListRoutes from './routes/shopping-list';

const DEFAULT_PORT = 3000;
const port = parseInt(process.env.PORT, 10) || DEFAULT_PORT;
const dev = process.env.NODE_ENV !== 'production';

const app = next({ dev });
const handle = app.getRequestHandler();

const createServer = () => {
  const server = express();

  server.use(compression());
  server.use(express.json());

  server.use(SHOPPING_LIST_ENDPOINT, shoppingListRoutes);
  server.get('*', (req, res) => {
    return handle(req, res);
  });

  return server;
};

let server;

initDb()
  .then(() => {
    server = createServer();
    return app.prepare();
  })
  .then(() => {
    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch(err => {
    console.log(err);
    process.exit(1);
  });

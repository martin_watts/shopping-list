import mongodb from 'mongodb';

const MongoClient = mongodb.MongoClient;

let _db;

export const initDb = () => {
  const mongoDbUrl = process.env.DB_CONN_STRING || 'mongodb://localhost:27017/shopping-list';

  if (_db) {
    return Promise.resolve(_db);
  }

  return MongoClient.connect(mongoDbUrl, { useNewUrlParser: true })
    .then(client => {
      _db = client;
      return _db;
    });
};

export const getDb = () => {
  if (!_db) {
    throw Error('Database not initialized');
  }
  return _db;
};

export const startSession = () => {
  const session = getDb().startSession();
  session.startTransaction();

  return session;
};

export const endSession = async(session, successfully) => {
  if (successfully) {
    await session.commitTransaction();
  } else {
    await session.abortTransaction();
  }
  session.endSession();
};

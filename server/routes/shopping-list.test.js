import { EventEmitter } from 'events';
import { ObjectId } from 'mongodb';
import httpMocks from 'node-mocks-http';
import MongoMemoryServer from 'mongodb-memory-server';
import status from 'http-status';

import { initDb, getDb } from '../db-client';

import shoppingListRoutes, {
  SHOPPING_LIST_COLLECTION,
  SHOPPING_LIST_ORDER_COLLECTION,
  JSON_CONTENT_TYPE
} from './shopping-list';

describe('shopping list server API', () => {
  let mockResponse;

  const mockItems = [
    {
      _id: new ObjectId(),
      title: 'Apple',
      marked: false
    },
    {
      _id: new ObjectId(),
      title: 'Orange',
      marked: true
    }
  ];

  const savedDbConnString = process.env.DB_CONN_STRING;
  let mongod;
  let replSet;

  beforeAll(async () => {
    mongod = new MongoMemoryServer();

    const uri = await mongod.getConnectionString();
    process.env.DB_CONN_STRING = uri;
    await initDb();
  });

  afterAll(async () => {
    process.env.DB_CONN_STRING = savedDbConnString;
    await replSet.stop();
    await mongod.shutdown();
  });

  beforeEach(async () => {
    mockResponse = httpMocks.createResponse({
      eventEmitter: EventEmitter
    });

    const db = getDb().db();

    await db.collection(SHOPPING_LIST_COLLECTION).deleteMany({});
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION).deleteOne({});
    await db.collection(SHOPPING_LIST_COLLECTION).insertMany(mockItems);
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION).insertOne({ rank: [mockItems[0]._id, mockItems[1]._id] });
  });
  test('gets all items on GET /', done => {
    // Assign
    const mockRequest = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    });

    const expectedItems = mockItems.map((item, index) => {
      return {
        ...item,
        _id: item._id.toString(),
        rank: index
      };
    });

    mockResponse.on('end', () => {
      // Assert
      expect(mockResponse._getData()).toEqual(JSON.stringify(expectedItems));
      expect(mockResponse._getStatusCode()).toBe(status.OK);
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });

  test('adds a new item on POST /', done => {
    const newId = new ObjectId();

    const newItem = {
      _id: newId.toString(),
      title: 'Hot cross buns',
      marked: false
    };

    // Assign
    const mockRequest = httpMocks.createRequest({
      method: 'POST',
      headers: JSON_CONTENT_TYPE,
      body: newItem,
      url: '/'
    });

    mockResponse.on('end', async() => {
      // Assert
      expect(mockResponse._getData()).toEqual(JSON.stringify({ message: 'List item added', itemId: newId.toString() }));
      expect(mockResponse._getStatusCode()).toBe(status.CREATED);

      const insertedItem = await getDb()
        .db()
        .collection(SHOPPING_LIST_COLLECTION)
        .findOne({
          _id: newId
        });
      const insertedRank = await getDb()
        .db()
        .collection(SHOPPING_LIST_ORDER_COLLECTION)
        .findOne({
          rank: newId
        });

      expect(insertedItem).toEqual({ ...newItem, _id: newId });
      expect(insertedRank).not.toBeNull();
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });

  test('deletes an item on DELETE /:id', done => {
    // Assign
    const mockRequest = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${mockItems[0]._id.toString()}`
    });

    mockResponse.on('end', async() => {
      // Assert
      expect(mockResponse._getStatusCode()).toBe(status.NO_CONTENT);

      const existingItem = await getDb()
        .db()
        .collection(SHOPPING_LIST_COLLECTION)
        .findOne({
          _id: mockItems[0]._id
        });
      const existingRank = await getDb()
        .db()
        .collection(SHOPPING_LIST_ORDER_COLLECTION)
        .findOne({
          rank: mockItems[0]._id
        });
        
      expect(existingItem).toBeNull();
      expect(existingRank).toBeNull();
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });

  test('updates an item on PATCH /:id', done => {
    // Assign
    const newMarked = !mockItems[1].marked;

    const mockRequest = httpMocks.createRequest({
      method: 'PATCH',
      headers: JSON_CONTENT_TYPE,
      body: {
        marked: newMarked
      },
      url: `/${mockItems[1]._id.toString()}`
    });

    mockResponse.on('end', async() => {
      // Assert
      expect(mockResponse._getStatusCode()).toBe(status.OK);

      const updatedItem = await getDb()
        .db()
        .collection(SHOPPING_LIST_COLLECTION)
        .findOne({
          _id: mockItems[1]._id
        });

      expect(updatedItem).toEqual({ ...mockItems[1], marked: newMarked });
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });

  test('fails update if a non-supported field is modified', done => {
    // Assign
    const mockRequest = httpMocks.createRequest({
      method: 'PATCH',
      headers: JSON_CONTENT_TYPE,
      body: {
        title: 'Bananas'
      },
      url: `/${mockItems[1]._id.toString()}`
    });

    mockResponse.on('end', async() => {
      // Assert
      expect(mockResponse._getStatusCode()).toBe(status.BAD_REQUEST);

      const existingItem = await getDb()
        .db()
        .collection(SHOPPING_LIST_COLLECTION)
        .findOne({
          _id: mockItems[1]._id
        });

      expect(existingItem).toEqual({ ...mockItems[1] });
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });

  test('re-orders items on PATCHing a new rank to /:id', done => {
    const newIndex = 0;

    // Assign
    const mockRequest = httpMocks.createRequest({
      method: 'PATCH',
      headers: JSON_CONTENT_TYPE,
      body: {
        rank: newIndex
      },
      url: `/${mockItems[1]._id.toString()}`
    });

    mockResponse.on('end', async() => {
      // Assert
      expect(mockResponse._getStatusCode()).toBe(status.OK);

      const updatedRank = await getDb()
        .db()
        .collection(SHOPPING_LIST_ORDER_COLLECTION)
        .findOne({
          rank: mockItems[1]._id
        });

      expect(updatedRank.rank).toEqual([
        mockItems[1]._id,
        mockItems[0]._id
      ]);
      done();
    });

    // Act
    shoppingListRoutes(mockRequest, mockResponse, () => {});
  });
});

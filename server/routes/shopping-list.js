import { Router as getRouter } from 'express';
import status from 'http-status';
import { ObjectId } from 'mongodb';

import { getDb, startSession, endSession } from '../db-client';

export const SHOPPING_LIST_COLLECTION = 'shoppingList';
export const SHOPPING_LIST_ORDER_COLLECTION = 'shoppingListOrder';
const router = getRouter();

const handleError = (err, res) => {
  console.log(err);
  res.status(status.INTERNAL_SERVER_ERROR).json({ message: 'An error occurred.' });
};

const getItems = async(_, res) => {
  const items = [];

  try {
  await getDb()
    .db()
    .collection(SHOPPING_LIST_ORDER_COLLECTION)
    .aggregate(
      { $unwind: { path: '$rank', includeArrayIndex: 'rank_order' } }, // Flatten the order array
      {
        $project: {
          // Map to an array objects where the _id  === _id from shopping list
          _id: '$rank',
          rank: '$rank_order'
        }
      },
      {
        $lookup: {
          // Left join to the shopping list collection (puts the shopping list item as an item in to the 'fullItem' array)
          from: SHOPPING_LIST_COLLECTION,
          localField: '_id',
          foreignField: '_id',
          as: 'fullItem'
        }
      },
      {
        $match: {
          // Ensure there was a join (inner join)
          fullItem: { $size: 1 }
        }
      },
      {
        $replaceRoot: {
          // Pick the returned item + rank position
          newRoot: {
            $mergeObjects: [{ $arrayElemAt: ['$fullItem', 0] }, { rank: '$rank' }]
          }
        }
      },
      {
        $project: {
          // Omit the fullItem
          fullItem: 0
        }
      }
    )
    .forEach(item => {
      items.push({
        ...item,
        _id: item._id.toString(),
        marked: !!item.marked
      });
    });

    res.status(status.OK).json(items);
  } catch (err) {
    handleError(err, res);
  }
};
router.get('/', getItems);

const createItem = async(req, res) => {
  const newItem = {
    _id: new ObjectId(req.body._id),
    title: req.body.title,
    marked: false
  };

  const session = startSession();
  const db = getDb().db();

  let insertResult;

  try {
    insertResult = await db.collection(SHOPPING_LIST_COLLECTION).insertOne(newItem, { session });
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION).findOneAndUpdate(
      {},
      { $push: { rank: newItem._id } },
      {
        upsert: true,
        session
      }
    );

    await endSession(session, true);
    res.status(status.CREATED).json({ message: 'List item added', itemId: insertResult.insertedId });
  } catch (err) {
    await endSession(session, false);
    handleError(err, res);
  }
};
router.post('', createItem);

const updateItemMarkedState = async (id, marked, res) => {
  const changeset = { marked };

  try {
    await getDb()
      .db()
      .collection(SHOPPING_LIST_COLLECTION)
      .updateOne({ _id: new ObjectId(id) }, { $set: changeset });

    res.status(status.OK).json({ message: 'Item updated', itemId: id, changeset });
  } catch (err) {
    handleError(err, res);
  }
};

const sortItem = async(id, newIndex, res) => {
  const changeset = { rank: newIndex };

  const session = startSession();

  const db = getDb().db();
  const dbId = new ObjectId(id);

  try {
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION)
    .findOneAndUpdate(
      {},
      {
        $pull: {
          rank: dbId
        }
      },
      {
        session
      }
    );
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION).findOneAndUpdate(
          {},
          {
            $push: {
              rank: {
                $each: [dbId],
                $position: newIndex
              }
            }
          },
          {
            session
          }
        );
    await endSession(session, true);
    res.status(status.OK).json({ message: 'List item re-ordered', itemId: id, changeset });
  } catch (err) {
    await endSession(session, false);
    handleError(err, res);
  }
};

const updateItem = (req, res) => {
  const id = req.params.id;
  const { marked, rank } = req.body;

  if (typeof marked !== 'undefined') {
    updateItemMarkedState(id, marked, res);
  } else if (typeof rank !== 'undefined') {
    sortItem(id, rank, res);
  } else {
    // The API doesn't support changing any other field value
    res.sendStatus(status.BAD_REQUEST);
  }
};
router.patch('/:id', updateItem);

const deleteItem = async(req, res) => {
  const session = startSession();

  const db = getDb().db();
  const dbId = new ObjectId(req.params.id);

  try {
    await db.collection(SHOPPING_LIST_ORDER_COLLECTION)
      .findOneAndUpdate(
        {},
        {
          $pull: {
            rank: dbId
          }
        },
        {
          session
        }
      );
    await db.collection(SHOPPING_LIST_COLLECTION).deleteOne({ _id: dbId });
    await endSession(session, true);
    res.sendStatus(status.NO_CONTENT);
  } catch (err) {
    await endSession(session, false);
    handleError(err, res);
  }
};
router.delete('/:id', deleteItem);

export default router;
